package be.eki.backend.DTO;

import be.eki.backend.model.user.User;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor
public class UserDTO {
    private String username;
    private int rating;

    public UserDTO(User user) {
    }
}

