package be.eki.backend.DTO;


import be.eki.backend.model.user.Message;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@NoArgsConstructor
public class MessageDTO {

    @Getter @Setter
    private String message;

    @Getter @Setter
    private Date sentDate;


    public MessageDTO(Message message) {
        this.message=message.getMessage();
        this.sentDate=message.getSentDate();
    }
}
