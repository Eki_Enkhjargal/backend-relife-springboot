package be.eki.backend.DTO;

import be.eki.backend.model.Address;
import be.eki.backend.model.add.Add;
import be.eki.backend.model.add.Bid;
import be.eki.backend.model.add.Reply;
import be.eki.backend.model.user.Conversation;
import be.eki.backend.model.user.Rating;
import be.eki.backend.model.user.User;
import lombok.Data;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

@Data
public class UserProfileDTO implements Serializable {
    private long id;
    private String username;

    private String email;

    private String firstName;

    private String lastName;

    private Date birthday;

    private Date joinDate;

    private float rating;

    private boolean activated;

    private String activationToken;

    public Set<Add> adds;

    private Collection<Add> favorites;

    private Collection<Reply> replies;

    private Collection<Bid> bids;

    private Collection<Conversation> conversations;

    private Collection<Rating> ratingsReceived;

    private String profilePictureUri;

    private Address address;
    public UserProfileDTO(User user) {
        this.id = user.getId();
        this.username = user.getUsername();
        this.email = user.getEmail();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.birthday = user.getBirthday();
        this.joinDate = user.getJoinDate();
        this.rating = user.getRating();
        this.profilePictureUri = user.getProfilePictureUri();
        this.address = user.getAddress();
    }

    public UserProfileDTO(User user, Set<Add> userAdds) {
        this.id = user.getId();
        this.username = user.getUsername();
        this.email = user.getEmail();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.birthday = user.getBirthday();
        this.joinDate = user.getJoinDate();
        this.rating = user.getRating();
        this.profilePictureUri = user.getProfilePictureUri();
        this.address = user.getAddress();
        this.adds = userAdds;
    }
}
