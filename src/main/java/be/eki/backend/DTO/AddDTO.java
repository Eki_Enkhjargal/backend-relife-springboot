package be.eki.backend.DTO;

import be.eki.backend.model.add.Add;
import be.eki.backend.model.add.Category;
import be.eki.backend.model.add.Status;
import lombok.*;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Data @NoArgsConstructor
public class AddDTO implements Serializable {

    private Long id;

    private String username;


    private Date listingDate;


    private String title;

    private String description;


    private Category category;

    private Status status;

    private Float askPrice;

    private Float highestBidPrice;

    private String photoUri;

    public AddDTO(Add add) {
        this.title=add.getTitle();
        this.description=add.getDescription();
        this.category=add.getCategory();
        this.id=add.getId();
        this.askPrice=add.getAskPrice();
        this.highestBidPrice=add.getHighestBidPrice();
        this.photoUri=add.getPhotoUri();
        this.status=add.getStatus();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AddDTO)) return false;
        AddDTO addDTO = (AddDTO) o;
        return Objects.equals(getId(), addDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "AddDTO{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", listingDate=" + listingDate +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", category=" + category +
                ", status=" + status +
                ", askPrice=" + askPrice +
                ", highestBidPrice=" + highestBidPrice +
                ", photoUri='" + photoUri + '\'' +
                '}';
    }
}
