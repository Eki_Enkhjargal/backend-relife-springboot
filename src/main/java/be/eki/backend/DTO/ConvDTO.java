package be.eki.backend.DTO;

import be.eki.backend.model.add.Add;
import be.eki.backend.model.user.Conversation;
import be.eki.backend.model.user.Message;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;


@NoArgsConstructor
public class ConvDTO {

    @Getter @Setter
    private Add add;
    @Getter @Setter
    private String subject;
    @Getter @Setter
    private List<MessageDTO>messages;


    public ConvDTO(Conversation conversation) {
        this.add = conversation.getAdd();
        this.subject=conversation.getSubject();
        this.messages=getMessages(conversation);
    }

    private List<MessageDTO> getMessages(Conversation conversation){
        List<MessageDTO> list=new ArrayList<>();

        for (Message message : conversation.getMessages()
             ) {
            list.add(new MessageDTO(message));
        }
        return list;

    }
}
