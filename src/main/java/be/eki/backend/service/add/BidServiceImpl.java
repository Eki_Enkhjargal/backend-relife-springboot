package be.eki.backend.service.add;

import be.eki.backend.model.add.Add;
import be.eki.backend.model.add.Bid;
import be.eki.backend.repo.add.AddRepository;
import be.eki.backend.repo.add.BidRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Transactional @Service @RequiredArgsConstructor @Log4j2
public class BidServiceImpl implements BidService {
    @NonNull
    BidRepository bidRepo;
    @NonNull
    AddRepository addRepository;

    @Override
    public Bid saveBid(Bid bid) {
        return bidRepo.save(bid);
    }

    @Override
    public List<Bid> getBids(long id) {

        Add add = addRepository.findById(id).get();
        return (List<Bid>) bidRepo.findByAdd(add);
    }
}
