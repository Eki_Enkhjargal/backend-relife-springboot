package be.eki.backend.service.add;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data @NoArgsConstructor
public class Filter {
    String title;
    String description;
    Date startDate;
    Date endDate;
}

enum SortBy {
    ASCENDING_DATE, DESCENDING_DATE;
}