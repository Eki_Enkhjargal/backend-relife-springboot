package be.eki.backend.service.add;


import be.eki.backend.model.add.Add;
import be.eki.backend.model.add.Category;
import be.eki.backend.model.user.User;

import java.util.Set;

public interface AddService {
    Add getAdd(Long id);
    Add saveAdd(Add add);
    void deleteAdd(Add add);
    Set<Add> getAdds();
    Set<Add> getUserAdds(User user);
    Set<Add> getAdds(String username);
    Set<Add> getAdds(Category category);
    Set<Add> getAdds(Filter filter);
}
