package be.eki.backend.service.add;

import be.eki.backend.model.add.Add;
import be.eki.backend.model.add.Reply;
import be.eki.backend.repo.add.ReplyRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.LinkedHashSet;
import java.util.Set;

@Transactional @Service @RequiredArgsConstructor @Log4j2
public class ReplyServiceImpl implements ReplyService {
    @NonNull
    ReplyRepository replyRepo;

    @Override
    public Reply getReply(long id) {
        if (replyRepo.findById(id).isPresent()) {
            return replyRepo.findById(id).get();
        }
        return null;
    }

    @Override
    public Reply saveReply(Reply reply) {
        return replyRepo.save(reply);
    }

    @Override
    public Set<Reply> getReplies(Add add) {
        Set<Reply> replies = new LinkedHashSet<>();
        replyRepo.findByAdd(add).forEach(replies::add);
        return replies;
    }

    @Override
    public void deleteReply(long id) {
        replyRepo.deleteById(id);
    }
}
