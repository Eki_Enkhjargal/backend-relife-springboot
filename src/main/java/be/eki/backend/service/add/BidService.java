package be.eki.backend.service.add;



import be.eki.backend.model.add.Bid;

import java.util.List;
import java.util.Set;

public interface BidService {
    Bid saveBid(Bid bid);
    List<Bid> getBids(long id);
}
