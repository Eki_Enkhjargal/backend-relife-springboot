package be.eki.backend.service.add;


import be.eki.backend.model.add.Add;
import be.eki.backend.model.add.Reply;

import java.util.Set;

public interface ReplyService {
    Reply getReply(long id);
    Reply saveReply(Reply reply);
    Set<Reply> getReplies(Add add);
    void deleteReply(long id);
}
