package be.eki.backend.service.add;

import be.eki.backend.model.add.Add;
import be.eki.backend.model.add.Category;
import be.eki.backend.model.user.User;
import be.eki.backend.repo.add.AddRepository;
import be.eki.backend.service.user.UserService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service @Transactional @RequiredArgsConstructor @Log4j2
public class AddServiceImpl implements AddService{
    @NonNull
    private AddRepository addRepo;
    @NonNull
    private UserService userService;

    @Override
    public Add saveAdd(Add add) {
        return addRepo.save(add);
    }

    public void deleteAdd(Add add) {
        addRepo.delete(add);
    }

    @Override
    public Set<Add> getAdds() {
        log.info("Fetch all");
        return StreamSupport.stream(addRepo.findAll().spliterator(), false)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Add> getUserAdds(User user) {
        addRepo.findByUser(user);
        return null;
    }

    @Override
    public Set<Add> getAdds(Category category) {
        return StreamSupport.stream(addRepo.findByCategory(category).spliterator(), false)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Add> getAdds(String username) {
        User user = userService.getByUsername(username);
        return StreamSupport.stream(addRepo.findByUser(user).spliterator(), false)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Add> getAdds(Filter filter) {
        return StreamSupport.stream(addRepo.findAddsByTitleContainingAndDescriptionContainingAndListingDateBetweenAndCategoryAndStatusAndAskPriceGreaterThanAndAskPriceLessThan(
                "","",null, null, null, null,0,Float.MAX_VALUE).spliterator(), false)
                .collect(Collectors.toSet());
    }

    @Override
    public Add getAdd(Long id) {
        if(addRepo.findById(id).isPresent()) {
            return addRepo.findById(id).get();
        }
        return null;
    }
}