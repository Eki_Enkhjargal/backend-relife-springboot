package be.eki.backend.service;

public interface MailService {
    void sendMail(String recipient, String subject, String message);
}