package be.eki.backend.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

@RequiredArgsConstructor @Log4j2
public class MailServiceImpl implements MailService{
    @NonNull
    JavaMailSender emailSender;

    @Override
    public void sendMail(String recipient, String subject, String body) {
        log.info("Sending mail to {}:\n{}", recipient, body);
        if (recipient == null || !recipient.contains("@")) {
            log.info("Invalid mail recipient");
            return;
        }
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("ReLifeBE@gmail.com");
        message.setTo(recipient);
        message.setSubject(subject == null ? "Re:life website" : subject);
        message.setText(body == null ? "" : body);
        log.info("Message made");
        try {
            emailSender.send(message);
            log.info("Message sent");
        } catch (Exception ex) {
            log.info(ex);
        }
    }
}
