package be.eki.backend.service.user;

import be.eki.backend.model.user.Conversation;
import be.eki.backend.model.user.Message;
import be.eki.backend.model.user.User;
import be.eki.backend.repo.user.ConversationRepository;
import be.eki.backend.repo.user.MessageRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Log4j2
@Service @Transactional
@RequiredArgsConstructor
public class ConversationServiceImpl implements ConversationService {
    @NonNull
    private ConversationRepository conversationRepository;
    @NonNull
    private MessageRepository messageRepository;
    @Override
    public List<Conversation> getConversations(User user) {
        List<Conversation> conversations = new ArrayList<>();
        conversationRepository.findConversationByParticipantsContaining(user).forEach(conversations::add);
        return conversations;
    }

    @Override
    public Conversation saveConversation(Conversation conversation) {
        log.info("Saving conversation: {}", conversation.getId());
        return conversationRepository.save(conversation);
    }

    @Override
    public void deleteConversation(Conversation conversation) {
        conversationRepository.delete(conversation);
    }

    @Override
    public Message saveMessage(Message message) {
        return messageRepository.save(message);
    }

    @Override
    public Message getMessage(Long id) {
        if (messageRepository.findById(id).isPresent()) {
            return messageRepository.findById(id).get();
        }
        log.info("Message id={} not found", id);
        return null;
    }

    @Override
    public void deleteMessage(Message message) {
        messageRepository.delete(message);
    }
}
