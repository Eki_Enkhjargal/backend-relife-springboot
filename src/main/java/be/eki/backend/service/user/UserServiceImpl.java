package be.eki.backend.service.user;

import be.eki.backend.model.user.Rating;
import be.eki.backend.model.user.Role;
import be.eki.backend.model.user.User;
import be.eki.backend.repo.user.RatingRepository;
import be.eki.backend.repo.user.RoleRepository;
import be.eki.backend.repo.user.UserRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service @Transactional @RequiredArgsConstructor @Log4j2
public class UserServiceImpl implements UserService, UserDetailsService {
    @NonNull
    private UserRepository userRepo;
    @NonNull
    private RoleRepository roleRepo;
    @NonNull
    private RatingRepository ratingRepo;
    @NonNull
    private PasswordEncoder passwordEncoder;
    @Setter
    private boolean passwordEncryptedByFrontend;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findByUsername(username);
        if(user == null ) {
            log.info("User not found: {}", username);
            throw new UsernameNotFoundException("User not found in db: ");
        } else {
            log.info("{} found in the db", user.getUsername());
        }
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        });
        return new org.springframework.security.core.userdetails.
                User(user.getUsername(), user.getPassword(), authorities);
    }

    @Override
    public boolean isEmailTaken(String email) {
        return userRepo.findByEmailAndActivated(email, true) != null;
    }

    @Override
    public User saveUser(User user) {
        if (!passwordEncryptedByFrontend) {
            log.info("Received unencrypted password for username={}, encrypting before saving", user.getUsername());
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }
        return userRepo.save(user);
    }

    @Override
    public void deleteUser(User user) {
        userRepo.delete(user);
    }

    @Override
    public Role createRole(Role role) {
        log.info("Saving new role: "+role);
        return roleRepo.save(role);
    }

    @Override
    public void deleteRole(Role role) {
        roleRepo.delete(role);
    }

    @Override
    public void addRole(String username, String roleName) {
        log.info("User {} will be {}", username, roleName);
        User user = userRepo.findByUsername(username);
        Role role = roleRepo.findByName(roleName);
        if(role == null ){
            roleRepo.save(new Role(roleName));
        }
        role = roleRepo.findByName(roleName);
        user.getRoles().add(role);
    }

    @Override
    public Rating saveRating(Rating rating) {
        return ratingRepo.save(rating);
    }

    @Override
    public void deleteRating(Rating rating) {
        ratingRepo.delete(rating);
    }

    @Override
    public User getByUsername(String username) {
        log.info("Fetch {}", username);
        return userRepo.findByUsername(username);
    }

    @Override
    public User getByEmail(String email) {
        log.info("Fetch {}", email);
        return userRepo.findByEmail(email);
    }

    @Override
    public User getByActivationToken(String token) {
        log.info("Fetch {}", token);
        return userRepo.findByActivationToken(token);
    }

    @Override
    public List<User> getUsers() {
        log.info("Fetch all");
        return StreamSupport.stream(userRepo.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    @Override
    public User getById(Long id) {
        if (userRepo.findById(id).isPresent()) {
            return userRepo.findById(id).get();
        }
        return null;
    }
}
