package be.eki.backend.service.user;


import be.eki.backend.model.user.Rating;
import be.eki.backend.model.user.Role;
import be.eki.backend.model.user.User;

import java.util.List;

public interface UserService {
    boolean isEmailTaken(String email);

    User saveUser(User user);
    void deleteUser(User user);
    List<User> getUsers();
    User getById(Long id);
    User getByUsername(String username);
    User getByEmail(String email);
    User getByActivationToken(String token);

    // User has Roles
    Role createRole(Role role);
    void deleteRole(Role role);
    void addRole(String username, String roleName);

    // User has Ratings
    Rating saveRating(Rating rating);
    void deleteRating(Rating rating);
}
