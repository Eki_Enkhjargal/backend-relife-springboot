package be.eki.backend.service.user;


import be.eki.backend.model.user.Conversation;
import be.eki.backend.model.user.Message;
import be.eki.backend.model.user.User;

import java.util.List;

public interface ConversationService {
    Conversation saveConversation(Conversation conversation);
    void deleteConversation(Conversation conversation);
    List<Conversation> getConversations(User user);

    // Conversation has Messages
    Message saveMessage(Message message);
    Message getMessage(Long id);
    void deleteMessage(Message message);
}
