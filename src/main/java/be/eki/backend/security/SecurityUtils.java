package be.eki.backend.security;

import java.security.Principal;

public class SecurityUtils {
    public static boolean isAnonymous(Principal principal) {
        return principal.getName().equals("anonymousUser");
    }
}