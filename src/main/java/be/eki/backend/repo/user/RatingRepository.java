package be.eki.backend.repo.user;


import be.eki.backend.model.user.Rating;
import be.eki.backend.model.user.User;
import org.springframework.data.repository.CrudRepository;

public interface RatingRepository extends CrudRepository<Rating, Long> {
    Iterable<Rating> findByUser(User user);
}
