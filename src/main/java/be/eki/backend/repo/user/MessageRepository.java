package be.eki.backend.repo.user;

import be.eki.backend.model.user.Conversation;
import be.eki.backend.model.user.Message;
import org.springframework.data.repository.CrudRepository;

public interface MessageRepository extends CrudRepository<Message, Long> {
    Iterable<Message> findByConversation(Conversation conversation);
}
