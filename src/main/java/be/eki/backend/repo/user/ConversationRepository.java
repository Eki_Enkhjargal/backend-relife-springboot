package be.eki.backend.repo.user;

import be.eki.backend.model.user.Conversation;
import be.eki.backend.model.user.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConversationRepository extends CrudRepository<Conversation, Long> {
    Conversation findConversationById(long id);
    Iterable<Conversation> findConversationByParticipantsContaining(User user);
}
