package be.eki.backend.repo.user;

import be.eki.backend.model.user.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);
    User findByEmail(String email);
    User findByActivationToken(String token);
    User findByRatingGreaterThanEqual(float rating);
    User findByEmailAndActivated(String email, boolean activated);
}
