package be.eki.backend.repo.add;

import be.eki.backend.model.add.Add;
import be.eki.backend.model.add.Bid;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface BidRepository extends CrudRepository<Bid, Long> {
    Optional<Bid> findById(Long id);
    Iterable<Bid> findByAdd(Add add);
}
