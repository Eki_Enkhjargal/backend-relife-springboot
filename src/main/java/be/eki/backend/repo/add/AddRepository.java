package be.eki.backend.repo.add;


import be.eki.backend.model.add.Add;
import be.eki.backend.model.add.Category;
import be.eki.backend.model.add.Status;
import be.eki.backend.model.user.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;

public interface AddRepository extends CrudRepository<Add, Long> {
    Iterable<Add> findAll();
    Iterable<Add> findByUser(User user);
    Long countAddsByUser(User user);
    Add removeAddById(long id);


    Iterable<Add> findByListingDateAfter(Date date);
    Iterable<Add> findByListingDateBefore(Date date);
    Iterable<Add> findByListingDateBetween(Date dateStart, Date dateStop);
    Iterable<Add> findByTitleContaining(String searchString);
    Iterable<Add> findByDescriptionContaining(String searchString);
    Iterable<Add> findByCategory(Category category);
    Iterable<Add> findByStatus(Status status);
    Iterable<Add> findByAskPriceGreaterThan(float askPrice);
    Iterable<Add> findByAskPriceLessThan(float askPrice);

    Iterable<Add> findAddsByTitleContainingAndDescriptionContainingAndListingDateBetweenAndCategoryAndStatusAndAskPriceGreaterThanAndAskPriceLessThan(
            String searchTitle, String searchDescription, Date firstDate, Date lastDate, Category category, Status status, float minPrice, float maxPrice
    );
}
