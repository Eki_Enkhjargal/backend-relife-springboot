package be.eki.backend.repo.add;

import be.eki.backend.model.add.Add;
import be.eki.backend.model.add.Reply;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ReplyRepository extends CrudRepository<Reply, Long> {
    Optional<Reply> findById(Long id);
    Iterable<Reply> findByAdd(Add add);
}
