package be.eki.backend;
import be.eki.backend.model.add.Add;
import be.eki.backend.model.user.Role;
import be.eki.backend.model.user.User;
import be.eki.backend.service.add.AddService;
import be.eki.backend.service.user.UserService;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication @Log4j2
public class SpringAddSiteApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(SpringAddSiteApplication.class, args);
    }


    CommandLineRunner createAdds(UserService userService, AddService addService) {
        return args -> {
            Add testAdd = new Add();
            testAdd.setTitle("Book");
            testAdd.setUser(userService.getByUsername("AEO"));
            addService.saveAdd(testAdd);
        };
    }

    @Bean
    CommandLineRunner createRoles(UserService userService, AddService addService) {
        return args -> {
            try {
                userService.createRole(new Role("ROLE_ADMIN"));
                userService.createRole(new Role("ROLE_ADULT"));
                userService.createRole(new Role("ROLE_MODERATOR"));
                userService.createRole(new Role("ROLE_RESTRICTED"));
            } catch (Exception ex) {
                ex.printStackTrace();
                log.info("Unable to create Role scheme, possible duplicate {}", ex.getMessage());
            }
        };
    }

    @Bean
    CommandLineRunner createUsers(UserService userService, AddService addService) {
        return args -> {
            try {
                User user = new User("AEO", "kevin.mommens@gmail.com", "password");
                userService.saveUser(user);
            } catch (Exception ex) {
                ex.printStackTrace();
                log.info("Unable to create user, possible duplicate {}", ex.getMessage());
            }
        };
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
