package be.eki.backend.model.add;

import be.eki.backend.DTO.AddDTO;
import be.eki.backend.model.user.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

@Entity @Table(name="adds") @NoArgsConstructor @RequiredArgsConstructor
public class Add implements Serializable {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) @Getter
    @Column(insertable = false, nullable = false, updatable = false)
    private Long id;

    @ManyToOne @Getter @Setter
    private User user;

    @JsonIgnore
    @OneToMany(mappedBy = "add") @Getter @Setter
    private Collection<Bid> bids;

    @Temporal(value = TemporalType.DATE) @Getter @Setter
    private Date listingDate = new Date();

    @Column(length = 25) @Getter @Setter @NonNull
    private String title;

    // ToDo: change mapping, length too large
    @Column(length = 1020) @Getter @Setter
    private String description;

    @Enumerated(EnumType.STRING) @Getter @Setter
    private Category category;

    @Enumerated(EnumType.STRING) @Getter @Setter
    private Status status;

    @Getter @Setter
    private Float askPrice;

    @Getter @Setter
    private Float highestBidPrice;

    @Getter @Setter
    private String photoUri;

    public Add(AddDTO addDTO) {
        this.setTitle(addDTO.getTitle());
        this.setDescription(addDTO.getDescription());
        this.setCategory(addDTO.getCategory());
        this.setPhotoUri(addDTO.getPhotoUri());
        this.setAskPrice(addDTO.getAskPrice());
        this.setHighestBidPrice(addDTO.getHighestBidPrice());
    }

   /* @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Add add = (Add) o;

        if (!user.equals(add.user)) return false;
        if (!listingDate.equals(add.listingDate)) return false;
        return title.equals(add.title);
    }

    @Override
    public int hashCode() {
        int result = user.hashCode();
        result = 31 * result + listingDate.hashCode();
        result = 31 * result + title.hashCode();
        return result;
    }*/
}