package be.eki.backend.model.add;

public enum Status {
    UNLISTED, LISTED, SOLD, DISPUTED, ARCHIVED;

}
