package be.eki.backend.model.add;

import java.io.Serializable;

public enum Category implements Serializable {
    ART, ANIMALS, APPAREL, AUDIO, BICYCLE, BOOKS, BUSINESS,
    CHILDREN, CD_DVD, COMPUTER, COLLECTION,
    ELECTRONICS, DIY, HOBBY, HOLIDAY, HOME,
    IMAGE, IMMO, MUSIC, OTHER,
    SERVICES, SPORT, SOFTWARE, VIDEO, VEHICLE
}
