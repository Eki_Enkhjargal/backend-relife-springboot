/*
 * Copyright (c) 2021-2021. Kevin Mommens. All Rights Reserved.
 */

package be.eki.backend.model.add;

import be.eki.backend.model.user.User;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity @Table(name = "replies") @NoArgsConstructor @RequiredArgsConstructor
public class Reply {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) @Getter
    @Column(insertable = false, nullable = false, updatable = false)
    private Long id;

    @ManyToOne @Getter @Setter
    private User user;

    @ManyToOne @Getter @NonNull
    private Add add;

    @Getter @Setter @NonNull
    private String reply;

    @Temporal(TemporalType.TIMESTAMP)
    private Date sentDate = new Date();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Reply reply1 = (Reply) o;

        if (!user.equals(reply1.user)) return false;
        if (!add.equals(reply1.add)) return false;
        if (!reply.equals(reply1.reply)) return false;
        return sentDate.equals(reply1.sentDate);
    }

    @Override
    public int hashCode() {
        int result = user.hashCode();
        result = 31 * result + add.hashCode();
        result = 31 * result + reply.hashCode();
        result = 31 * result + sentDate.hashCode();
        return result;
    }
}
