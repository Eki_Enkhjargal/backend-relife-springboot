package be.eki.backend.model.add;

import be.eki.backend.model.user.User;
import lombok.*;

import javax.persistence.*;


@Entity @Table(name = "bids") @NoArgsConstructor @RequiredArgsConstructor
public class Bid {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) @Getter
    @Column(insertable = false, nullable = false, updatable = false)
    private Long id;

    @ManyToOne @Setter @Getter @NonNull
    // This is set by Spring Security,the logged in Principal, when bidding on an add.
    private User user;

    @ManyToOne @Getter @Setter @NonNull
    private Add add;

    @Getter @Setter @NonNull
    private float bidPrice;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bid bid = (Bid) o;

        if (Float.compare(bid.bidPrice, bidPrice) != 0) return false;
        if (!user.equals(bid.user)) return false;
        return add.equals(bid.add);
    }

    @Override
    public int hashCode() {
        int result = user.hashCode();
        result = 31 * result + add.hashCode();
        result = 31 * result + (bidPrice != +0.0f ? Float.floatToIntBits(bidPrice) : 0);
        return result;
    }
}
