package be.eki.backend.model.user;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity @Table(name = "roles") @NoArgsConstructor @RequiredArgsConstructor
public class Role implements Serializable {
    @Id @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(insertable = false, nullable = false, updatable = false) @Getter
    private Long id;

    @Column(unique = true, nullable = false) @NonNull @Getter @Setter
    private String name;
    @ManyToOne @Getter @Setter
    // This is set by Spring Security Principal.
    private User user;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;

        return name.equals(role.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
