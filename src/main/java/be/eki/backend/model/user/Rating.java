/*
 * Copyright (c) 2021-2021. Kevin Mommens. All Rights Reserved.
 */

package be.eki.backend.model.user;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity @Table(name = "ratings") @NoArgsConstructor @RequiredArgsConstructor
public class Rating {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) @Getter
    @Column(insertable = false, nullable = false, updatable = false)
    private Long id;

    @Getter @Setter
    private int rating;

    @ManyToOne @Getter @Setter @NonNull
    private User user;

    @OneToOne @Getter @Setter
    private User reviewer;

    @Getter @Setter
    private String comment;

    @Temporal(value = TemporalType.DATE) @Column(nullable = false) @Getter @Setter
    private Date sentDate = new Date();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rating that = (Rating) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
