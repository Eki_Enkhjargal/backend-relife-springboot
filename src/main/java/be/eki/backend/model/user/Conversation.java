package be.eki.backend.model.user;

import be.eki.backend.model.add.Add;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;

@Entity @Table(name = "conversations") @NoArgsConstructor
public class Conversation implements Serializable {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) @Getter
    @Column(insertable = false, updatable = false, nullable = false)
    private Long id;

    @OneToOne @Getter @Setter
    private Add add;

    @Getter @Setter
    private String subject;

    @ManyToMany @Getter @Setter
    private Collection<User> participants;

    @OneToMany(mappedBy = "conversation") @Getter @Setter
    private Collection<Message> messages;

    public Conversation(Add add, User... users) {
        this.participants.addAll(Arrays.asList(users));
    }

    public Conversation(String subject, User... users) {
        this.participants.addAll(Arrays.asList(users));
    }

    public void addParticipants(User... users) {
        this.participants.addAll(Arrays.asList(users));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Conversation conversation = (Conversation) o;

        return id.equals(conversation.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}