package be.eki.backend.model.user;

import be.eki.backend.model.Address;

import be.eki.backend.model.add.Add;
import be.eki.backend.model.add.Bid;
import be.eki.backend.model.add.Reply;
import lombok.*;
import lombok.extern.log4j.Log4j2;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.*;

import static javax.persistence.FetchType.EAGER;

@Entity @Table(name="users") @NoArgsConstructor @Log4j2
public class User {
    @Id @GeneratedValue(strategy= GenerationType.IDENTITY) @Getter
    @Column(insertable = false, nullable = false, updatable = false)
    private long id;

    @Column(nullable = false, unique = true, length = 20) @Getter @Setter
    private String username;

    @Column(nullable = false, length = 45) @Getter @Email
    private String email;

    public String setEmail(String newEmail) {
        if (!this.email.equals(newEmail)) {
            log.info("Changing mail from {} to {}", this.email, newEmail);
            this.email = newEmail;
            this.activated = false;
            this.activationToken = UUID.randomUUID().toString();
        } else {
            log.info("New email is equal to old mail");
        }

        return this.email;
    }

    @Column(nullable = false) @Getter @Setter
    private String password;

    @Column(length = 20) @Getter @Setter
    private String firstName;

    @Column(length = 20) @Getter @Setter
    private String lastName;

    @ManyToMany(fetch = EAGER) @Getter @Setter
    Collection<Role> roles;

    @Temporal(value = TemporalType.DATE) @Getter @Setter
    private Date birthday;

    @Temporal(value = TemporalType.DATE) @Getter @Setter
    private Date joinDate = new Date();

    @Getter @Setter
    private float rating;

    @Getter @Setter
    private boolean activated;

    @Column(nullable = false) @Getter
    private String activationToken;

    @ManyToMany @Getter @Setter
    private Collection<Add> favorites;

    @OneToMany(mappedBy = "user") @Getter @Setter
    private Collection<Reply> replies;

    @ManyToMany @Getter @Setter
    private Collection<Bid> bids;

    @ManyToMany @Getter @Setter
    private Collection<Conversation> conversations;

    @OneToMany(mappedBy = "user") @Getter @Setter
    private Collection<Rating> ratingsReceived;

    @Getter @Setter
    private String profilePictureUri;

    @OneToOne @Getter @Setter
    private Address address;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    public User(String username,String email,String password) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.activationToken = UUID.randomUUID().toString();
    }
}
