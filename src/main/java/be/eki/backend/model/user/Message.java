

package be.eki.backend.model.user;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity @Table(name = "messages")
public class Message {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, updatable = false, nullable = false) @Getter
    private Long id;

    // assigned from SpringSecurity in MessageResource.java
    @OneToOne @Getter @Setter
    private User sender;

    @Getter @Setter
    private String message;

    @ManyToOne @Getter @Setter
    private Conversation conversation;

    @Temporal(value = TemporalType.DATE) @Getter @Setter
    private Date sentDate = new Date();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message that = (Message) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
