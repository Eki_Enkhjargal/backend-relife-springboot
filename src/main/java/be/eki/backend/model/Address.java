package be.eki.backend.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity @Table(name = "addresses") @NoArgsConstructor
public class Address {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, updatable = false, nullable = false) @Getter
    private Long id;

    @Getter @Setter
    private String streetName;

    @Getter @Setter
    private Integer number;

    @Getter @Setter
    private String addition;

    @Getter @Setter
    private String bus;

    @Getter @Setter
    private String postalCode;

    @Getter @Setter
    private String city = "Ghent";
    
    @Getter @Setter
    private String country = "Belgium";
}
