package be.eki.backend.api;

import be.eki.backend.DTO.AddDTO;
import be.eki.backend.model.add.Add;
import be.eki.backend.model.add.Bid;
import be.eki.backend.model.add.Category;
import be.eki.backend.model.add.Reply;
import be.eki.backend.model.user.User;
import be.eki.backend.service.add.AddService;
import be.eki.backend.service.add.BidService;
import be.eki.backend.service.add.Filter;
import be.eki.backend.service.add.ReplyService;
import be.eki.backend.service.user.UserService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.ServletRequest;
import java.net.URI;
import java.security.Principal;
import java.util.*;

@Log4j2
@CrossOrigin(origins = "http://localhost:4200")
@RestController @RequestMapping("/api") @RequiredArgsConstructor
public class AddResource {
    @NonNull
    private AddService addService;
    @NonNull
    private UserService userService;
    @NonNull
    private ReplyService replyService;

    @NonNull
    private BidService bidService;

    @GetMapping("/adds")
    public ResponseEntity<Set<AddDTO>> getAdds(ServletRequest request) {
        Set<AddDTO> addDTOs=new HashSet<>();
        String categoryRequested = request.getParameter("cat");
        if (categoryRequested != null) {
            Category category = Category.valueOf(categoryRequested.toUpperCase(Locale.ROOT));
            log.info("Fetching all adds, category=(requested={},foundInDB={})", categoryRequested, category);
            addService.getAdds(category).forEach(add -> {
                addDTOs.add(new AddDTO(add));
            });
            return ResponseEntity.ok().body(addDTOs);
        }
        String usernameRequested = request.getParameter("username");
        if (usernameRequested != null) {
            log.info("Fetching all adds for username={}", usernameRequested);
            addService.getAdds(usernameRequested).forEach(add -> {
                addDTOs.add(new AddDTO(add));
            });
            return ResponseEntity.ok().body(addDTOs);
        }
        String titleRequested = request.getParameter("title");
        if (titleRequested != null) {
            log.info("Fetching all adds with title containing title={}", titleRequested);
            Filter filter = new Filter();
            filter.setTitle(titleRequested);
            addService.getAdds(filter).forEach(add -> {
                addDTOs.add(new AddDTO(add));
            });
            return ResponseEntity.ok().body(addDTOs);
        }
        log.info("Fetching all adds");

        addService.getAdds().forEach(add -> {
            addDTOs.add(new AddDTO(add));
        });
        log.info("fetching {} adds ", addDTOs.size());
        return ResponseEntity.ok().body(addDTOs);
    }

    @GetMapping("/add/{id}")
    public ResponseEntity<AddDTO> getAddById(@PathVariable("id") long id) {
        log.info("Fetching {} add", id);
        Add add = addService.getAdd(id);
        if (add != null) {
            return ResponseEntity.ok(new AddDTO(add));

        }
        return ResponseEntity.status(org.springframework.http.HttpStatus.BAD_REQUEST).build();
    }

    @PostMapping("/add")
    public ResponseEntity<AddDTO> saveAdd(@RequestBody Add add, Principal principal) {
        log.info("Saving addTitle={} for principal={}",add.getTitle(), principal);
        if (principal != null) {
            User requester = userService.getByUsername(principal.getName());
            add.setUser(requester);
          //  add.setPhotoUri(UploadResource.photoUri);
        } else {
            log.info("Unable to add Add=(title={}), no logged in User found",add.getTitle());
            return ResponseEntity.status(org.springframework.http.HttpStatus.UNAUTHORIZED).build();
        }
        User requester = userService.getByUsername(principal.getName());
        log.info("Saving addTitle={} for user={}",add.getTitle(), requester.getUsername());
        add.setUser(requester);
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/add").toUriString());
        return ResponseEntity.created(uri).body(new AddDTO(addService.saveAdd(add)));
    }

    @PutMapping("/add/{id}")
    public ResponseEntity<Boolean> updateAdd(@PathVariable("id") long id, @RequestBody AddDTO addDTO, Principal principal) {
        System.out.println("from put**************");
        User requester = userService.getByUsername(principal.getName());
        Add addRequested = addService.getAdd(id);
        if (addRequested !=null) {
            addRequested.setHighestBidPrice(addDTO.getHighestBidPrice());
            addRequested.setUser(requester);
            addRequested.setAskPrice(addDTO.getAskPrice());
            addRequested.setPhotoUri(addDTO.getPhotoUri());
            addRequested.setCategory(addDTO.getCategory());
            addRequested.setDescription(addRequested.getDescription());
            addRequested.setTitle(addDTO.getTitle());
            addService.saveAdd(addRequested);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @DeleteMapping("/add/{id}")
    public ResponseEntity<Boolean> deleteAddById(@PathVariable("id") long id, Principal principal) {
        log.info("Deleting addId={} for principal={}", id, principal);
        if (principal != null) {
            User requester = userService.getByUsername(principal.getName());
            Add add = addService.getAdd(id);
            if (add != null && requester != null){
                //if (add.get)
            }
        } else {
            //log.info("Unable to add Add=(title={}), no logged in User found",add.getTitle());
            return ResponseEntity.status(org.springframework.http.HttpStatus.UNAUTHORIZED).build();
        }
        Add add = addService.getAdd(id);
        if (add != null) {
            addService.deleteAdd(add);
            return ResponseEntity.ok().build();
        }
        log.info("Unable to delete addId={}, not found in DB", id);
        return ResponseEntity.status(org.springframework.http.HttpStatus.BAD_REQUEST).build();
    }

    @PostMapping("/add/reply")
    public ResponseEntity<Reply> saveAddReply(@RequestBody Reply reply, Principal principal) {
        if (principal == null ||principal.getName().equals("anonymousUser")) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        User requester = userService.getByUsername(principal.getName());
        reply.setUser(requester);
        log.info("Saving reply {}\non add {}\nfor {}", reply.getReply(), reply.getAdd(), requester.getUsername());
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/add/reply").toUriString());
        return ResponseEntity.created(uri).body(replyService.saveReply(reply));
    }

    // for Bids:
    @PostMapping("/add/bid")
    public ResponseEntity<Bid> saveAddBid(@RequestBody Bid bid, Principal principal) {
        if (principal == null ||principal.getName().equals("anonymousUser")) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        User requester = userService.getByUsername(principal.getName());
        bid.setUser(requester);
        Add add=addService.getAdd(bid.getAdd().getId());
        add.getBids().add(bid);
        addService.saveAdd(add);
        log.info("Saving bid {}\non add {}\nfor {}", bid.getBidPrice(), bid.getAdd(), requester.getUsername());
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/add/bid").toUriString());
        return ResponseEntity.created(uri).body(bidService.saveBid(bid));
    }

    @GetMapping("/add/bid/{id}")
    public ResponseEntity<List<Bid>> getAddBids(@PathVariable("id") long id) {
       List<Bid> bids=bidService.getBids(id);
       bids.sort(Comparator.comparing(Bid::getBidPrice));
       Collections.reverse(bids);
       return ResponseEntity.ok().body(bids);
    }

    @DeleteMapping("/add/reply/{id}")
    public ResponseEntity<Boolean> deleteReplyById(@PathVariable("id") long id, Principal principal) {
        log.info("Deleting replyId={} for principal={}", id, principal);
        if (principal == null ||principal.getName().equals("anonymousUser")) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        Reply reply = replyService.getReply(id);
        User requester = userService.getByUsername(principal.getName());
        if (reply != null && requester != null && requester.equals(reply.getUser())) {
            replyService.deleteReply(id);
            log.info("Deleted replyId={}", id);
            return ResponseEntity.ok().build();
        }
        log.info("Unable to delete replyId={}, not found in DB", id);
        return ResponseEntity.status(org.springframework.http.HttpStatus.BAD_REQUEST).build();
    }

    @GetMapping("/useradds")
    public ResponseEntity<Set<AddDTO>> getAdds(Principal principal) {
        if (principal == null ||principal.getName().equals("anonymousUser")) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        Set<AddDTO> addDTOs =  new HashSet<>();
                addService.getAdds(principal.getName()).forEach(add -> {
            addDTOs.add(new AddDTO(add));
        });
        return ResponseEntity.ok().body(addDTOs);
    }
}