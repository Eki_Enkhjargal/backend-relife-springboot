package be.eki.backend.api;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.*;


@RestController @RequestMapping("/api") @Log4j2
public class UploadResource {
    //private static String uploadFolder = "//uploads//";
    private static String uploadFolder = System.getProperty("user.dir") + "/src/main/resources/uploads/";

    private static Set<String> allowedExtensions = new HashSet<>(Arrays.asList("img","IMG","png","PNG","gif","GIF"));
    public static String photoUri="";

  // @PostMapping("/upload")
    public ResponseEntity<String> singleFileUpload(@RequestParam("imageFile") MultipartFile file, Principal principal) {
       System.out.println("file: " + file.getName()+ " ++++++++++++");
        log.info("uploading");
        if (principal == null || principal.getName().equals("anonymousUser")) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        log.info("Hello");
        if (file.isEmpty()) {
            return ResponseEntity.badRequest().header("error", "empty file").build();
        }
        try {
            byte[] bytes = file.getBytes();
            if (file.getOriginalFilename() == null) {
                return ResponseEntity.badRequest().header("error", "no file name").build();
            }
            String[] fileParts = file.getOriginalFilename().split("[.]");
            String extension;
            try {
                extension = fileParts[fileParts.length-1];
                if (!allowedExtensions.contains(extension)) {
                    return ResponseEntity.badRequest().header("error", "invalid extension").build();
                }
            } catch (IndexOutOfBoundsException ex) {
                ex.printStackTrace();
                return ResponseEntity.badRequest().header("error", "no extension found").build();
            }
            String fileName = UUID.randomUUID() + "." + extension;
            photoUri=fileName;
            Path path = Paths.get(uploadFolder + fileName);
            Files.write(path, bytes);
            return ResponseEntity.ok(fileName);
        } catch (IOException ex) {
            ex.printStackTrace();
            return ResponseEntity.badRequest().header("error", ex.getMessage()).build();
        }
    }
}