package be.eki.backend.api;

import be.eki.backend.DTO.UserDTO;
import be.eki.backend.DTO.UserProfileDTO;
import be.eki.backend.model.user.User;
import be.eki.backend.service.add.AddService;
import be.eki.backend.service.user.UserService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.ServletRequest;
import java.net.URI;
import java.security.Principal;
import java.util.*;

@RestController @RequestMapping("/api") @RequiredArgsConstructor @Log4j2
@CrossOrigin(origins = "http://localhost:4200") // ToDo: CORS
public class UserResource {
    @NonNull
    private final UserService userService;
    @NonNull
    private final AddService addService;

    @GetMapping("/users")
    public ResponseEntity<Set<UserDTO>> getUsers(ServletRequest request) {
        Set<UserDTO> users = new HashSet<UserDTO>();
        userService.getUsers().forEach(user -> {
            users.add(new UserDTO(user));
        });
        return ResponseEntity.ok().body(users);
    }

    @PostMapping("/user") // ?username= && ?email= && ?password=
    public ResponseEntity<UserProfileDTO> createUser(ServletRequest request) {
        String username = request.getParameter("username");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        if (username == null || email == null || password == null) {
            log.info("Unable to create User, missing parameter: username={}, email={} or passwordLength={}",
                    username, email, password == null ? -1 : password.length());
            return ResponseEntity.badRequest().header("error",
                    String.format("Error creating User, missing parameter: username=%s, email=%s or passwordLength=%s",
                    username, email, password == null ? -1 : password.length())).build();
        }
        if (userService.isEmailTaken(email)) {
            return ResponseEntity.badRequest().header("error",
                    String.format("Email=%s is taken", email)).build();
        }
        if (userService.getByUsername(username) != null) {
            return ResponseEntity.badRequest().header("error",
                    String.format("Username=%s is taken", email)).build();
        }
        log.info("Creating User with username={} email={} passwordLength={}", username, email, password.length());
        User user = new User(username, email, password);
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/user").toUriString());
        return ResponseEntity.created(uri).body(new UserProfileDTO(userService.saveUser(user)));
    }

    @PutMapping("/user")
    public ResponseEntity<UserDTO> updateUser(@RequestBody UserProfileDTO userInfo, Principal principal) {
        if (principal != null) {
            log.info("Updating User information for Principal={}", principal == null ? "NO_PRINCIPAL_FOUND" : principal.getName());
            User currentUser = userService.getByUsername(principal.getName());
            currentUser.setAddress(userInfo.getAddress());
            currentUser.setBirthday(userInfo.getBirthday());
            URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/user").toUriString());
            return ResponseEntity.created(uri).body(new UserDTO(userService.saveUser(currentUser)));
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @DeleteMapping("/user/{id}")
    public ResponseEntity<Boolean> deleteUser(@PathVariable("id") long id, Principal principal) {
        User user = userService.getById(id);
        if (user != null) {
            log.info("Deleting User(id={},username={}) for username={}", id, user.getUsername(), principal.getName());
            userService.deleteUser(user);
            return ResponseEntity.ok().build();
        }
        log.info("Unable to delete, user(id={}) not found in db", id);
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<UserDTO> getUserById(@PathVariable("id") long id, Principal principal) {
        User user = userService.getById(id);
        if (user != null) {
            log.info("Fetched  id={} username={}", id, user.getUsername());
            return ResponseEntity.ok(new UserDTO(user));
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @GetMapping("/user") // ?username= || ?verify=
    public ResponseEntity<UserDTO> getUser(ServletRequest request, Principal principal) {
        User user = null;
        String activationToken = request.getParameter("verify");
        String usernameRequested = request.getParameter("username");
        if (activationToken != null) {
            log.info("Activating user with token: {}", activationToken);
            user = userService.getByActivationToken(activationToken);
            user.setActivated(true);
            userService.saveUser(user);
            return ResponseEntity.ok(new UserDTO(user));
        }
        if (usernameRequested != null) {
            log.info("Fetching user with username: {}", usernameRequested);
            user = userService.getByUsername(usernameRequested);
            return ResponseEntity.ok(new UserDTO(user));
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @GetMapping("/profile")
    public ResponseEntity<UserProfileDTO> getUserProfile(ServletRequest request, Principal principal) {
       if(principal==null){
           return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
       }
        User user = userService.getByUsername(principal.getName());
        if (user != null) {
            return ResponseEntity.ok(new UserProfileDTO(user,addService.getUserAdds(user)));
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }
}