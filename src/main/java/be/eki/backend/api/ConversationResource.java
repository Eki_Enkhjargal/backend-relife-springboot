package be.eki.backend.api;

import be.eki.backend.model.user.Conversation;
import be.eki.backend.model.user.Message;
import be.eki.backend.model.user.User;
import be.eki.backend.service.user.ConversationService;
import be.eki.backend.service.user.UserService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import java.security.Principal;
import java.util.*;

@RestController @RequestMapping("/api") @RequiredArgsConstructor @Log4j2
public class ConversationResource {
    @NonNull
    UserService userService;
    @NonNull
    ConversationService conversationService;

    @GetMapping("conversation")
    public ResponseEntity<List<Conversation>> getConversations(ServletRequest request, Principal principal) {
        User requester = userService.getByUsername(principal.getName());
        int limit = 100;
        log.info("Fetching max={} conversations for username={}",limit, requester.getUsername());
        List<Conversation> conversations = new ArrayList<>();
        Iterable<Conversation> conversationsFound = conversationService.getConversations(requester);
        conversationsFound.forEach(conversations::add);
        log.info("Found size={} conversations", conversations.size());
        return ResponseEntity.ok(conversations);
    }

    @PostMapping("message")
    public ResponseEntity<Message> saveMessage(Message message, Principal principal) {
        User requester = userService.getByUsername(principal.getName());
      //  log.info("Saving message={} on conversationId={} for username={}",message.getMessage(), message.getConversation().getId(), requester.getUsername());
        return ResponseEntity.ok(conversationService.saveMessage(message));
    }

    @GetMapping("message/{id}")
    public ResponseEntity<Message> getMessage(@PathVariable("id") long id, Principal principal) {
        User requester = userService.getByUsername(principal.getName());
        log.info("Fetching messageId={} for username={}", id, requester.getUsername());
        return ResponseEntity.ok(conversationService.getMessage(id));
    }
}
